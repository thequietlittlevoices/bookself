class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.text :content
      t.string :book
      t.string :author
      t.references :user

      t.timestamps
    end
  end
end
