class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use   :null_session instead.
  protect_from_forgery with: :null_session
  before_action :authenticate_user!

  def after_sign_in_path_for(resource)
  	resource.update cached_failed_attempts: 0, failed_attempts: 0
    stored_location_for(resource) || signed_in_root_path(resource)
  end
end
