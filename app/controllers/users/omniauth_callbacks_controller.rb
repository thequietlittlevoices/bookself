class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
 
   
     @user = User.from_omniauth(request.env["omniauth.auth"])
     #puts "user errors: #{@user.errors.to_a}"

    if @user.persisted?
      puts "Hello"
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    else
     
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      
      flash.now[:alert] = "Account cannot be created with the current credentials: #{@user.email} has already been taken."
      render '/users/sessions/new'
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
end