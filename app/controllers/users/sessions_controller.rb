class Users::SessionsController < Devise::SessionsController
  def create
    flash.clear
    user = User.find_by_email(sign_in_params['email'])
    super and return unless user

    adjust_failed_attempts user

    super and return if (user.failed_attempts < User.logins_before_captcha)

    if !verify_recaptcha and recaptcha_present?(params)
      decrement_failed_attempts(user)
      self.resource = resource_class.new(sign_in_params)
      clean_up_passwords(resource)
      sign_out
      flash[:alert]="Captcha code incorrect."
      respond_with resource, location: after_sign_in_path_for(resource)
    else
      if user.access_locked?
        super and return
      else
        flash.delete :recaptcha_error
        super and return
      end
    end    
  end

 private

 def adjust_failed_attempts(user)
    if user.failed_attempts > user.cached_failed_attempts
      user.update cached_failed_attempts: user.failed_attempts
    else
      increment_failed_attempts(user)
    end
  end

  def increment_failed_attempts(user)
    user.increment :cached_failed_attempts
    user.update failed_attempts: user.cached_failed_attempts
  end

  def decrement_failed_attempts(user)
    if user.cached_failed_attempts>(User.logins_before_captcha+1)
      user.decrement :cached_failed_attempts 
      user.update failed_attempts: user.cached_failed_attempts
    end
  end


  def recaptcha_present?(params)
    if params["g-recaptcha-response"]
      return true
    else
      return false
    end
  end
end