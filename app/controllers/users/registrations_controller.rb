class Users::RegistrationsController < Devise::RegistrationsController

 #  prepend_before_filter :must_not_be_omniauth_user, only: [:edit]


  def edit
   if user_signed_in? and not current_user.provider.blank?
   	 flash[:alert]="Editing your profile is not allowed when signed in external provider."
     redirect_to :quotes
   else
     super
    end
  end

  private
  def after_inactive_sign_up_path_for(resource)
    new_user_session_path
  end
end