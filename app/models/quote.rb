class Quote < ActiveRecord::Base
  belongs_to :user
  validates  :content, :book, :author, presence: true
end
