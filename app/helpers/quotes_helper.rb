module QuotesHelper
  def created_at_format(quote)
    quote.created_at.strftime("%d %b. %Y   %H:%M")
  end
end
