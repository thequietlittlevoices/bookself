module ApplicationHelper

  def errors_for(object)
	  concat(content_tag(:ul) do
	    object.errors.full_messages.each do |msg|
	      concat content_tag(:li, msg)
	    end
      end)
  end
end
